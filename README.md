# Erenoit's st
This is my fork of suckless's st. Original git repo is [here](https://git.suckless.org/st/).

## What is st?
st (simple terminal) is a simple terminal emulator for X which sucks less.


## Requirements
In order to build st you need the Xlib header files.

### Fonts
- [Iosevka](https://github.com/be5invis/iosevka)
- [Sarasa](https://github.com/be5invis/Sarasa-Gothic)
- [JoyPixels](https://www.joypixels.com/)

## Installation
Edit config.mk to match your local setup (st is installed into the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if necessary as root):

```sh
make clean install
```

If you did not install st with make clean install, you must compile the st terminfo entry with the following command:

```sh
tic -sx st.info
```

See the man page for additional details.

## Configuration
The configuration of st is done by creating a custom config.h and (re)compiling the source code.

## Credits
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.

## Patches
All the patches are included in `patches/` folder.
- [alpha](https://st.suckless.org/patches/alpha/)
- [anysize](https://st.suckless.org/patches/anysize/)
- [boxdraw](https://st.suckless.org/patches/boxdraw/)
- [csi 22 23](https://st.suckless.org/patches/csi_22_23/)
- [desktopopentry](https://st.suckless.org/patches/desktopentry/)
- [dynamic cursor color](https://st.suckless.org/patches/dynamic-cursor-color/)
- [font2](https://st.suckless.org/patches/font2/)
- [glyph wide support](https://st.suckless.org/patches/glyph_wide_support/)
- [hidecursor](https://st.suckless.org/patches/hidecursor/)
- [ligatures](https://st.suckless.org/patches/ligatures/) **boxdraw version**
- [undercurl](https://st.suckless.org/patches/undercurl/)
- [w3m](https://st.suckless.org/patches/w3m/)

